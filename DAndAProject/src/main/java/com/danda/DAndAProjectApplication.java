package com.danda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DAndAProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DAndAProjectApplication.class, args);
	}

}
